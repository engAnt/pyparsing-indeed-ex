from pyparsing import makeHTMLTags, Word, alphas, Literal, OneOrMore, SkipTo, Suppress, Group, alphanums
from urllib.request import urlopen

jobUrl = "http://www.indeed.com/viewjob?jk=a49924448b23afc0&tk=1b40mooscaqhqemd&from=company"
page = urlopen(jobUrl)
pageContent = page.read()
page.close()

'''trStart,trEnd = makeHTMLTags("tr")
tableRow = SkipTo( trEnd )

for row in tableRow.scanString(pageContent):
    print(row, "\n")'''


ulStart,ulEnd = makeHTMLTags("ul")
liStart,liEnd = makeHTMLTags("li")
pStart,pEnd = makeHTMLTags("p")
boldStart,boldEnd = makeHTMLTags("b")

titleText = Group( OneOrMore(Word(alphas)) )
colon = Suppress(Literal(":"))
titleStart = pStart + boldStart
titleEnd = colon + boldEnd + pEnd
sectionTitle = titleStart + titleText.setResultsName("sTitle") + titleEnd

listText = Group( OneOrMore( Word(alphas, alphanums+"; , : / ( ) -") ) )
liBody = liStart + listText + liEnd
sectionBody = ulStart + OneOrMore( liBody ) + ulEnd

section = sectionTitle + sectionBody.setResultsName("sBody")

'''for tTxt, start, end in sectionTitle.scanString(pageContent):
    print(tTxt.sTitle)
    #print(sT, "\n")'''

for tokens, start, end in section.scanString(pageContent):
    print(tokens.sTitle, "\n", tokens.sBody, "\n\n")